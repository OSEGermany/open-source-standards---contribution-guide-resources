Open Source Standards – Update Policy
===

[TOC]

# Terms and Definitions

## Consortium
- group of people working on the official new release of the standard
- consists of at least 1 representative of each [Stakeholder Category](#Stakeholder Category)
- includes at least one representative of a [Conservative Force](#Conservative-Force-wants-to-preserve)
- exists for a limited amount of time, between the establishment of the consortium and until the final approval of the new version of the standard
- every member in the consortium has one vote they can make use of for each decision
- decisions are made with a full [Consent](#Consent) except where otherwise stated
- is represented by a consortuium leader and a consortium co-leader

## Stakeholder Category
- represents a group of people affected by the standard (e.g. OSH developers, OSH collaboration platform)
- generally share a common perspective on the standard (e.g. work with it in a certain way)
- the list of considered Stakeholder Categories is defined by the consortium (stakeholder categories may change with the evolution of the standard)

## Conservative Force
- is a person or group of people who have been part of a previous consortium, for example:
	- a former consortium member of one of the last 2 official releases of the standard
	- a representative of an organisation maintaining this standard (e.g. DIN e.V.) 
- probably carries the philosophy of previous consortiums and is aware of old goals
- may want to preserve the itegrity of the previous version of the standard and rather oppose and vote against changes
	
## Transformative Force
- is a person or a group of people who was not involved in the previous releases of the standard, for example:
	- a new representative of a previous stakeholder category
	- a representative of a new stakeholder category
- brings in a new perspective
- may rather be in favour and vote for changes
- is probably not aware of all backgrounds of decisions made in previous consortiums
	
## Consent
- consent (no one votes _agains_ it) ≠ consensus (everyone votes _for_ it)
- a vote can be
	- positive (no disagreement) → alright!
	- emotionally/vaguely negative (no specified reason, just a feeling) → should be noticed
	- negative (for a specified reason) → needs to be discussed (…and resolved, when a full consent is required)

# The Rules 

## § 0 Submission to Licensing Terms and Local Law

"Licensing terms" refer to the license under which the standard is published.

## § 1 General transparency
All relevant actions need to be captured and published so that anyone (regardless of the practical effort) can follow all decisions made by the consortium.

## § 2 General openness
1. Everyone is invited to participate. The consortium remains open until the final approval of the new version of the standard. 

2. Joining the consortium is free of charge. The only requirements applying are the following. A new member:
	1. commits to active contribution and declares disposing of the reqiured resources to support their active contribution
	2. commits using the workflows and tools agreed upon by the consortium
	3. is endorsed by the consortium. The consortium decides in a meeting, in which the potential new member is present, about the accession.
	4. represents a stakeholder category
	5. represents either a Transformative or Conservative Force
    
## § 3 Building a new Consortium
1. Given that there is a practical need to officially update the standard, someone organises an initial meeting of the new Consortium (kick-off). 
2. All potential Conservative Forces need to be notified about the establishment of a new Consortium. This needs to be proven (e.g. by a server status notification with a successful mail delivery). Note that there's no need to prove that the notification has reached its recipient.
3. During the kickoff, attendees may commit to the active contribution for the new release of the standard. 
4. This forms the new Consortium if the requirements stated in the [Definitions](#Terms-and-Definitions) above are met. If not, The potential new Consortium either
	1. decides about a change of the stakeholder categories so that the requirements are met (given that none of the potential Conservative Forces blocks this)
or
	2. creates a fork of the standard instead of an update
or
	3. disbands.

## § 5 Public feedback loop
A draft can only be published as the new official release of the standard if
1. at least 4 weeks of time have been given to the general public to give feedback on the draft to adopt
2. the consortium has made decisions on each point of the feedback given by the general public.

## § 6 In case of conflicts
1. Members can be permanentaly excluded from the Consortium by a majority decision of $\geq \frac{2}{3}$ of all members.
2. Excluded members cannot join this Consortium again.
3. Excluded members don't qualify as a Conservative Force for future Consortiums.

# Pracical cases

## OKH metadata standard

### stakeholder categories
- OSH developer
- OSH upload platform
- OSH search engine
- OSH replicator
- OSH user

## DIN SPEC 3105

## OSH Guideline

